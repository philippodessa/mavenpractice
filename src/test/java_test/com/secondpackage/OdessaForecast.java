package com.secondpackage;

import java.util.concurrent.TimeUnit;

import com.testclasses.BrowserFactory;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.Test;
import com.testclasses.Methods;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;




public class OdessaForecast {

    public WebDriver driver;
    Methods actions ;

    @BeforeClass
    public void init() {
        driver = BrowserFactory.getBrowser("Firefox");
        actions =  new Methods(driver);
        driver.manage().window().fullscreen();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
    }

    @Test (priority = 6)
    public  void navigate () {
        driver.get("https://sinoptik.ua");
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }


    @Test (priority = 7)
    public void findCity () throws InterruptedException {
        new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#search_city")));
        driver.findElement(By.cssSelector("#search_city")).sendKeys("одесса");
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".search_city-submit")).click();
        Thread.sleep(2000);
    }


    @Test (priority = 8)
    public void checkHeading () throws InterruptedException {
        new WebDriverWait(driver, 40).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("h1")));
        Assert.assertTrue(driver.findElement(By.cssSelector("h1")).getText().toLowerCase().contains("в одессе"));
    }


    @Test (priority = 9)
    public void switch10Days () throws InterruptedException {
        driver.findElement(By.cssSelector("#topMenu a")).click();
        Thread.sleep(2500);
    }

    @Test (priority = 10)
    public void get10DaysForecast () throws InterruptedException {
        actions.print10DaysForecast();
    }


    @AfterClass
    public void closeBrowser() throws InterruptedException {
        actions.closeWindow();
    }

}
