package com.testclasses;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.util.List;


public class Methods {

    WebDriver driver ;
    public Methods (WebDriver currentBrowser) {
        System.out.println(currentBrowser);
        driver = currentBrowser;
    }

    public void waitForPageLoaded() {
        ExpectedCondition<Boolean> expectation = new
                ExpectedCondition<Boolean>() {
                    public Boolean apply(WebDriver driver) {
                        return ((JavascriptExecutor) driver).executeScript("return document.readyState").toString().equals("complete");
                    }
                };
        try {
            Thread.sleep(1000);
            WebDriverWait wait = new WebDriverWait(driver, 30);
            wait.until(expectation);
        } catch (Throwable error) {
            Assert.fail("Timeout waiting for Page Load Request to complete.");
        }
    }




    public void print10DaysForecast () throws InterruptedException {
        List<WebElement> days = driver.findElements(By.cssSelector(".tabs .main"));

        for (int i =0 ; i<days.size(); i++) {
            days.get(i).click();
            Thread.sleep(500);

            String date = days.get(i).findElement(By.cssSelector(".date")).getText();
            String month = days.get(i).findElement(By.cssSelector(".month")).getText();
            System.out.println("Forecast for "+date + "  "+ month);
            System.out.println("Min temperature is :  "+days.get(i).findElement(By.cssSelector(".min span")).getText());
            System.out.println("Max temperature is :  "+days.get(i).findElement(By.cssSelector(".max span")).getText());

           if (checkForRain(i))
               System.out.println("Rain is expected. You should take umbrella");

           else
               System.out.println("Rain is not expected");

            printSpaces(1);
        }

    }


    public void printSpaces (int quantityStrings) {
        for (int i =0 ; i<quantityStrings; i++){
            System.out.println("");
        }

    }


    public boolean checkForRain (int index) {
        Boolean rain = false ;

        List <WebElement> tabsDetails = driver.findElements(By.cssSelector(".tabsContentInner .Tab"));
        List <WebElement> timeWeather = tabsDetails.get(index).findElements(By.cssSelector(".weatherDetails .weatherIco"));
        for (int j = 0; j<timeWeather.size(); j++ ){
            if ( timeWeather.get(j).getAttribute("title").contains("дождь")){
                rain = true;
                break;
            }
        }
        return rain;
    }

    public void closeWindow ()  {
        this.driver.close();
    }



}
