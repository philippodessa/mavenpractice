package com.testclasses;

import com.testclasses.Methods;
import org.omg.PortableServer.THREAD_POLICY_ID;
import org.openqa.selenium.*;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.*;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

import static com.sun.tools.doclint.Entity.image;


public class DownloadImage {

//   public WebDriver driver = BrowserFactory.getBrowser("ChromeWithOptions");
//   Methods action = new Methods(driver);
    public WebDriver driver;
    Methods actions ;

    @BeforeClass
    public void init () {
       driver = BrowserFactory.getBrowser("ChromeWithOptions");
       actions =  new Methods(driver);
       driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
       driver.manage().window().fullscreen();
    }

    @Test (priority = 20)
    public void downloadFile () throws InterruptedException, AWTException {
        //navigate to your page
        driver.get("http://dumskaya.net/news/dayana-yastremskaya-intervyu-091528/");
        new WebDriverWait(driver, 30).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".content")));

        WebElement element =   driver.findElements(By.cssSelector("a.piclink")).get(0);
        ((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);

        WebElement myImage = driver.findElement(By.cssSelector(".lb-image"));

        JavascriptExecutor js;
        if (driver instanceof JavascriptExecutor) {
            js = (JavascriptExecutor)driver;
        } else {
            throw new IllegalStateException("This driver does not support JavaScript!");
        }


        js.executeScript("   $(`<a class=\"test-link\" href=\"/pics1/c0-1540456938.jpg\"\n" +
                "onclick=\"this.href = $('[href=\"/pics1/c0-1540456938.jpg\"]').attr('src');\"\n" +
                "download>Click here to download image</a>`).insertAfter($($('[href=\"/pics1/c0-1540456938.jpg\"]')))   ");


        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".test-link")).click();
    }

    @AfterClass
    public void closeBrowser() throws InterruptedException {
        actions.closeWindow();
//        BrowserFactory.deleteAllDrivers();
    }

//    @AfterSuite
//    public void tearDown() {
//        BrowserFactory.deleteAllDrivers();
//    }




}
