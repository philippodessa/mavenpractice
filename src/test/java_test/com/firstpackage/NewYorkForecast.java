package com.firstpackage;

import java.nio.file.Paths;
import java.util.concurrent.TimeUnit;

import com.testclasses.Methods;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.testclasses.BrowserFactory;

public class NewYorkForecast {

    public WebDriver driver;
    Methods actions ;

    @BeforeClass
    public void init() throws InterruptedException {
        driver = BrowserFactory.getBrowser("Chrome");
        actions =  new Methods(driver);
        Thread.sleep(1000);
        driver.manage().window().fullscreen();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
}

    @Test (priority = 1)
    public  void navigate () {
        driver.get("https://sinoptik.ua");
        driver.manage().timeouts().pageLoadTimeout(15, TimeUnit.SECONDS);
    }


    @Test (priority = 2, dependsOnMethods = "navigate")
    public void findCity () throws InterruptedException {
        new WebDriverWait(driver, 15).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector("#search_city")));
        driver.findElement(By.cssSelector("#search_city")).sendKeys("Нью-Йорк");
        Thread.sleep(1000);
        driver.findElement(By.cssSelector(".search_city-submit")).click();
        Thread.sleep(4000);
    }


    @Test (priority = 3 , dependsOnMethods = "findCity")
    public void checkHeading () throws InterruptedException {
        new WebDriverWait(driver, 40).until(ExpectedConditions.presenceOfElementLocated(By.cssSelector(".cityName")));
        Assert.assertTrue(driver.findElement(By.cssSelector(".cityName h1")).getText().toLowerCase().contains("в нью-йорке"));
    }


    @Test (priority = 4)
    public void switch10Days () throws InterruptedException {
        driver.findElement(By.cssSelector("#topMenu a")).click();
        Thread.sleep(2500);
    }

    @Test (priority = 5 , dependsOnMethods = "switch10Days")
    public void get10DaysForecast () throws InterruptedException {
        actions.print10DaysForecast();
    }


    @AfterClass
    public void closeBrowser()  {
      actions.closeWindow();
    }

}
